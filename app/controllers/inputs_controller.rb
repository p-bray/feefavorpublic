class InputsController < ApplicationController

    def create 

        @input = Input.new(create_input_params)

        @input.save

        redirect_to pages_home_path
        
    end

    def create_input_params
    
        params.require(:input).permit(:description)

    end
end
